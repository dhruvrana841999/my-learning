(function ($) {
    Drupal.behaviors.carousel = {
        attach: function (context, settings) {
            $('.owl-slider-wrapper', context).each(function () {
                var $this = $(this);
                var $this_settings = $.parseJSON($this.attr('data-settings'));
                $this.owlCarousel($this_settings);
            });

        }
    };

    jQuery('.owl-slider-wrapper').owlCarousel({
			loop:true,
			margin:10,
			responsiveClass:true,
			responsive:{
			0:{
				items:2,
				nav:true
			},
			767:{
				items:3,
				nav:false
			},
			992:{
				items:4,
				nav:true,
				loop:false
			},
			1200:{
				items:5,
				nav:true,
				loop:false
		}
	}
});
	// if ( $(window).width() < 767) {
 //            $('.owl-responsive').owlCarousel({
	// 			responsiveClass: true,
	// 			margin: 0,
	// 			items: 1,
 //                nav: true,
 //                responsive: {
 //                  0: {
 //                    items: 1,
 //                    nav: true,
	// 				margin: 0,
	// 				autoplay: true,
	// 				loop: true,
	// 				dots: false
 //                  },
 //                  480: {
 //                    items: 2,
 //                    nav: true,
	// 				margin: 20,
	// 				autoplay: true,
	// 				loop: true,
	// 				dots: false
 //                  }
 //                }

 //            });

	// };
})(jQuery);